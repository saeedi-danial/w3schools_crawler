<?php

ini_set('max_execution_time', 100000);

//DB Connection
$conn = new PDO("mysql:host=localhost;dbname=w3schools", "root", "");

include('../simplehtmldom_1_5/simple_html_dom.php');
$html = new simple_html_dom();

$stmt = $conn->prepare("SELECT * FROM `menu`");
$stmt->execute();
$menu = $stmt->fetchAll();

//Capture pages
$page_capture = $conn->prepare("INSERT INTO `pages`(`html_holder`,`sub_menu_id`) VALUES (:page_content,1)");
$page_capture->bindParam(":page_content",$page_content);


$page = file_get_contents("http://www.w3schools.com/html/".$menu[0]['link']);
$html->load($page);
$page_content = $html->find("#main")[0];

for($i = 0; $i < count($menu);$i++){
	$page = file_get_contents("http://www.w3schools.com/html/".$menu[$i]['link']);
	$html->load($page);
	$page_content = $html->find("#main")[0];

	$new_image = null;
	$page_images = $html->find("#main img");
	$image_holder = [];
	for($j = 0; $j < count($page_images);$j++){
		$explode = explode("/",$page_images[$j]->src);
		if(count($explode) == 1){
			$new_image = "src='http://www.w3schools.com/html/".$page_images[$j]->src."'";
		}else{
			$new_image = "src='http://www.w3schools.com".$page_images[$j]->src."'";
		}

		$image_holder[$j] = $new_image;
	}

	$preg_statement = '#src="([^"]*)"#';
	$image_id = -1;
	$page_content = preg_replace_callback( 
	    $preg_statement, 
	    function($match) use ($image_holder) {global $image_id; $image_id += 1; return (($match[0] = $image_holder[$image_id])); }, 
	    $page_content 
	    );
	$page_capture->execute();
}






